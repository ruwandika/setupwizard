package au.com.seebo.setupwizard.timezone;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import au.com.seebo.setupwizard.R;

/**
 * Created by Ruwa on 5/17/17.
 */

public class LanguageAdapter extends BaseAdapter{

    private ArrayList<LanguageModel> languageData;
    Context context;
    private static LayoutInflater inflater = null;

    public LanguageAdapter(Context context, ArrayList<LanguageModel> languageData) {
        this.context = context;
        this.languageData = languageData;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return languageData.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public class Holder{
        TextView textLanguage, textLocale;
        ImageView selected;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Holder holder = new Holder();
        View rowView;
        rowView = inflater.inflate(R.layout.language_list, null);
        holder.textLanguage = (TextView) rowView.findViewById(R.id.textViewLanguage);
        holder.textLocale = (TextView) rowView.findViewById(R.id.textViewLocale);
        holder.selected = (ImageView) rowView.findViewById(R.id.imageViewSelected);

        holder.textLanguage.setText(languageData.get(position).getLanguage());
        holder.textLocale.setText(languageData.get(position).getLocale());
        if(languageData.get(position).isSelected()){
            holder.selected.setVisibility(View.VISIBLE);
        } else {
            holder.selected.setVisibility(View.INVISIBLE);
        }

        return rowView;
    }
}













