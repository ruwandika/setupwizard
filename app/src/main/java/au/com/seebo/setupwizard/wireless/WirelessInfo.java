package au.com.seebo.setupwizard.wireless;

/**
 * Created by Ruwa on 5/15/17.
 */

public class WirelessInfo {

    private String ssid;
    private String ipAddress;
    private String subnetMask;
    private String defaultGateway;
    private String dns;

    public WirelessInfo(String ssid, String ipAddress, String subnetMask, String defaultGateway, String dns) {
        this.ssid = ssid;
        this.ipAddress = ipAddress;
        this.subnetMask = subnetMask;
        this.defaultGateway = defaultGateway;
        this.dns = dns;
    }

    public String getSsid() {
        return ssid;
    }

    public void setSsid(String ssid) {
        this.ssid = ssid;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getSubnetMask() {
        return subnetMask;
    }

    public void setSubnetMask(String subnetMask) {
        this.subnetMask = subnetMask;
    }

    public String getDefaultGateway() {
        return defaultGateway;
    }

    public void setDefaultGateway(String defaultGateway) {
        this.defaultGateway = defaultGateway;
    }

    public String getDns() {
        return dns;
    }

    public void setDns(String dns) {
        this.dns = dns;
    }
}
