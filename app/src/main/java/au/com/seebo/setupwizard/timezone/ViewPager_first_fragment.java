package au.com.seebo.setupwizard.timezone;

import android.app.AlarmManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import au.com.seebo.setupwizard.MainActivity;
import au.com.seebo.setupwizard.R;

/**
 * Created by Ruwa on 5/16/17.
 */

public class ViewPager_first_fragment extends Fragment{

    private Button langNext, back;
    private TimeZoneAdapter timeZoneAdapter;
    private Context context;
    private ListView lv;
    private TextView txtStatus;
    private View root;
    private ArrayList<TimeZoneModel> timeZoneData;

    public static Fragment newInstance(Context context) {

        ViewPager_first_fragment f = new ViewPager_first_fragment();

        return f;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if(root == null){
            root = inflater.inflate(R.layout.viewpager_first_fragment, container, false);
        } else {
            ViewGroup parent = (ViewGroup) root.getParent();
            parent.removeView(root);
        }

        langNext = (Button) root.findViewById(R.id.buttonNext);
        back = (Button) root.findViewById(R.id.button_back);
        lv = (ListView) root.findViewById(R.id.list);
        txtStatus = (TextView) root.findViewById(R.id.textStatus);
        txtStatus.setText("Loading Time Zones..");
        selectTimeZone();
        doButtonClick();

        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        getTimeZones();
        /*timeZoneAdapter = new TimeZoneAdapter(getActivity(),timeZoneData);
        lv.setAdapter(timeZoneAdapter);*/
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    private void doButtonClick(){
        langNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), MainActivity.class));
                getActivity().finish();
            }
        });
    }

    private void selectTimeZone(){
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                TextView timeZoneId = (TextView) view.findViewById(R.id.textViewTimeZoneId);
                TextView gmtVal = (TextView) view.findViewById(R.id.textViewGMT);
                ImageView selected = (ImageView) view.findViewById(R.id.imageViewSelected);

                for(int i=0; i<timeZoneData.size(); i++){
                    timeZoneData.get(i).setSelected(false);
                }
                timeZoneData.get(position).setSelected(true);
                timeZoneAdapter.notifyDataSetChanged();
                // ----- Change Time Zone -----
                TimeZoneModel data = timeZoneData.get(position);
                changeTimeZone(data.getTimeZoneId());
                Toast.makeText(getActivity().getApplicationContext(),
                        ""+data.getTimeZoneId(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void getTimeZones(){

        new Thread(){
            @Override
            public void run() {
                try {
                    //--
                    timeZoneData = new ArrayList<>();
                    TimeZoneModel timezoneObj;

                    String[] ids = TimeZone.getAvailableIDs();
                    for(String id: ids){

                        timezoneObj = formatTimeZone(TimeZone.getTimeZone(id));
                        timeZoneData.add(timezoneObj);

                    }
                    //--
                    handler.sendEmptyMessage(0);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }.start();

    }
    Handler handler = new Handler(){

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            txtStatus.setText("");
            timeZoneAdapter = new TimeZoneAdapter(getActivity(),timeZoneData);
            lv.setAdapter(timeZoneAdapter);
            //lv.setSelection(0);
            //timeZoneData.get(0).setSelected(true);
            //timeZoneAdapter.notifyDataSetChanged();
        }
    };


    private TimeZoneModel formatTimeZone(TimeZone tz){

        long hours = TimeUnit.MILLISECONDS.toHours(tz.getRawOffset());
        long minutes = TimeUnit.MILLISECONDS.toMinutes(tz.getRawOffset())
                - TimeUnit.HOURS.toMinutes(hours);

        // avoid -4:-30 issue
        minutes = Math.abs(minutes);

        String formattedGMT = String.format("(GMT+%d:%02d)", hours, minutes);
        String id = tz.getID();

        TimeZoneModel obj = new TimeZoneModel(id, formattedGMT);

        return obj;

    }

    private void changeTimeZone(String timeZoneID){
        AlarmManager am = (AlarmManager) getActivity().getApplicationContext().getSystemService(Context.ALARM_SERVICE);
        am.setTimeZone(timeZoneID);

        Toast.makeText(getActivity().getApplicationContext(), "new: "+TimeZone.getDefault().getDisplayName(), Toast.LENGTH_SHORT).show();

    }

   /* @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("left_arrow");
        intentFilter.addAction("right_arrow");
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(mMessageReceiver, intentFilter);
    }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            //------ do -------
            if(intent.getAction().equals("left_arrow"))
                back.requestFocus();

            if(intent.getAction().equals("right_arrow"))
                langNext.requestFocus();

        }
    };*/

    //---
}












