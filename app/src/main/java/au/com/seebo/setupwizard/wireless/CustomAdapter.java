package au.com.seebo.setupwizard.wireless;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import au.com.seebo.setupwizard.R;

/**
 * Created by Ruwa on 5/8/17.
 */

public class CustomAdapter extends BaseAdapter {

    ArrayList<String> connName;
    ArrayList<String> connStatus;
    ArrayList<Integer> conImage;
    Context context;
    private static LayoutInflater inflater = null;

    public CustomAdapter(Context context, ArrayList<String> connName, ArrayList<String> connStatus,ArrayList<Integer> conImage) {
        this.context = context;
        this.connName = connName;
        this.connStatus = connStatus;
        this.conImage = conImage;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return connName.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public class Holder{
        TextView connSSID, connStatus;
        ImageView connImg;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        Holder holder = new Holder();
        View rowView;
        rowView = inflater.inflate(R.layout.connection_list, null);
        holder.connSSID = (TextView) rowView.findViewById(R.id.textViewConnName);
        holder.connImg = (ImageView) rowView.findViewById(R.id.imageViewConnImg);
        holder.connStatus = (TextView) rowView.findViewById(R.id.textViewConnStatus);

        holder.connSSID.setText(connName.get(position));
        holder.connStatus.setText(connStatus.get(position));
        holder.connImg.setImageResource(conImage.get(position));
        rowView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, "Network: "+connName.get(position), Toast.LENGTH_SHORT).show();
            }
        });

        return rowView;
    }
}