package au.com.seebo.setupwizard.timezone;

/**
 * Created by Ruwa on 5/17/17.
 */

public class LanguageModel {

    private String language, locale;
    private boolean isSelected;

    public LanguageModel(String language, String locale) {
        this.language = language;
        this.locale = locale;
    }

    public LanguageModel(String language, String locale, boolean isSelected) {
        this.language = language;
        this.locale = locale;
        this.isSelected = isSelected;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}
