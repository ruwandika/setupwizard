package au.com.seebo.setupwizard.utils;

import android.util.Log;

/**
 * Created by Ruwa on 5/8/17.
 */

public class MyLog {

    static String tag = "check";

    public static void doLog(String msg){
        Log.d(tag, msg);
    }

    public static void doLog(String tagSuffix, String msg){
        Log.d(tag+" "+tagSuffix, msg);
    }
}
