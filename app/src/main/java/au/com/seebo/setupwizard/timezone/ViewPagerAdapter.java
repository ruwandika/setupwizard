package au.com.seebo.setupwizard.timezone;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

/**
 * Created by Ruwa on 5/16/17.
 */

public class ViewPagerAdapter extends FragmentPagerAdapter
{


    private Context _context;
    public static int totalPage=2;

    public ViewPagerAdapter(Context context, FragmentManager fm) {
        super(fm);
        _context=context;

    }

    @Override
    public Fragment getItem(int position) {
        Fragment f = new Fragment();
        switch(position){
            case 0:
                f=ViewPager_first_fragment.newInstance(_context);
                break;
            case 1:
                f=ViewPager_second_fragment.newInstance(_context);
                break;


        }
        return f;
    }
    @Override
    public int getCount() {
        return totalPage;
    }


}