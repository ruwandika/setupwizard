package au.com.seebo.setupwizard.wired;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import au.com.seebo.setupwizard.MainActivity;
import au.com.seebo.setupwizard.R;

/**
 * Created by Ruwa on 5/15/17.
 */

public class WiredMain extends Activity {

    private TextView mainText;
    private Button close;
    private Context context;
    private ImageView circleIndicator1, circleIndicator2, circleIndicator3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_wired_main);

        context = this;
        mainText = (TextView) findViewById(R.id.textStatus);
        close = (Button) findViewById(R.id.wired_close);

        goBackHome();
        //--
        circleIndicator1 = (ImageView) findViewById(R.id.indicator1);
        circleIndicator2 = (ImageView) findViewById(R.id.indicator2);
        circleIndicator3 = (ImageView) findViewById(R.id.indicator3);
        //--
        circleIndicator1.setBackgroundResource(R.drawable.off_circle);
        circleIndicator2.setBackgroundResource(R.drawable.on_circle);
        circleIndicator3.setBackgroundResource(R.drawable.off_circle);

    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    private void goBackHome(){
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(WiredMain.this, MainActivity.class));
                WiredMain.this.finish();
            }
        });
    }

}


















