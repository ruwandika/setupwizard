package au.com.seebo.setupwizard.timezone;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import au.com.seebo.setupwizard.R;

/**
 * Created by Ruwa on 5/16/17.
 */

public class TimeZoneAdapter extends BaseAdapter{

    private ArrayList<TimeZoneModel> timeZoneData;
    /*ArrayList<String> timeZoneId;
    ArrayList<String> valGMT;*/
    Context context;
    private static LayoutInflater inflater = null;

    public TimeZoneAdapter(Context context, ArrayList<TimeZoneModel> timeZoneData) {
        this.context = context;
        this.timeZoneData = timeZoneData;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return timeZoneData.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public class Holder{
        TextView textTimeZone, textGMT;
        ImageView selected;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        Holder holder = new Holder();
        View rowView;
        rowView = inflater.inflate(R.layout.timezone_list, null);
        holder.textTimeZone = (TextView) rowView.findViewById(R.id.textViewTimeZoneId);
        holder.textGMT = (TextView) rowView.findViewById(R.id.textViewGMT);
        holder.selected = (ImageView) rowView.findViewById(R.id.imageViewSelected);

        holder.textTimeZone.setText(timeZoneData.get(position).getTimeZoneId());
        holder.textGMT.setText(timeZoneData.get(position).getValGMT());
        if(timeZoneData.get(position).isSelected()){
            holder.selected.setVisibility(View.VISIBLE);
        } else
            holder.selected.setVisibility(View.INVISIBLE);

        rowView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, "Time Zone: "+timeZoneData.get(position).getTimeZoneId(), Toast.LENGTH_SHORT).show();
            }
        });

        return rowView;
    }
}
