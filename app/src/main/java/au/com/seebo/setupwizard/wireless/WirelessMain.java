package au.com.seebo.setupwizard.wireless;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.text.InputType;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import au.com.seebo.setupwizard.MainActivity;
import au.com.seebo.setupwizard.R;
import au.com.seebo.setupwizard.utils.MyLog;

/**
 * Created by Ruwa on 5/8/17.
 */

public class WirelessMain extends Activity{

    private TextView mainText;
    private WifiManager mainWifi;
    private WifiReceiver receiverWifi;
    private List<ScanResult> wifiList;
    public static int PERMISSIONS_REQUEST_CODE_ACCESS_COARSE_LOCATION = 1;
    public static int PERMISSIONS_REQUEST_CODE_ACCESS_WIFI_STATE = 2;
    private Button close;
    private Context context;
    private ListView lv;
    private ArrayList<String> connList;
    private ArrayList<String> connStatusList;
    private ArrayList<Integer> connImgList;
    private CustomAdapter customAdapter;
    private WifiInfo wifiInfo;
    private String currentSSID;
    private ImageView circleIndicator1, circleIndicator2, circleIndicator3;
    private Intent conIntent;
    private ProgressDialog progressDialog;
    private WirelessInfo wirelessInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_wireless_main);

        context = this;
        mainText = (TextView) findViewById(R.id.textStatus);
        close = (Button) findViewById(R.id.wireless_close);
        lv = (ListView) findViewById(R.id.list);

        mainWifi = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        if (mainWifi.isWifiEnabled() == false)
        {
            // If wifi disabled then enable it
            Toast.makeText(getApplicationContext(), "wifi is disabled..making it enabled",
                    Toast.LENGTH_LONG).show();
            mainWifi.setWifiEnabled(true);
        }
        receiverWifi = new WifiReceiver();

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(WifiManager.NETWORK_STATE_CHANGED_ACTION);
        intentFilter.addAction(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION);
        intentFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
        registerReceiver(receiverWifi, intentFilter);

        mainWifi.startScan();
        mainText.setVisibility(View.VISIBLE);
        mainText.setText("Starting Scan...");
        selectNetwork();
        goBackHome();

        circleIndicator1 = (ImageView) findViewById(R.id.indicator1);
        circleIndicator2 = (ImageView) findViewById(R.id.indicator2);
        circleIndicator3 = (ImageView) findViewById(R.id.indicator3);

        circleIndicator1.setBackgroundResource(R.drawable.on_circle);
        circleIndicator2.setBackgroundResource(R.drawable.off_circle);
        circleIndicator3.setBackgroundResource(R.drawable.off_circle);
    }

    protected void onPause() {

        unregisterReceiver(receiverWifi);
        super.onPause();
        MyLog.doLog("onPause");
    }

    protected void onResume() {
        MyLog.doLog("onResume");
        connList = new ArrayList<>();
        connStatusList = new ArrayList<>();
        connImgList = new ArrayList<>();

        wifiList = new ArrayList<>();
        wifiInfo = mainWifi.getConnectionInfo();
        currentSSID = wifiInfo.getSSID();

            receiverWifi = new WifiReceiver();
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction(WifiManager.NETWORK_STATE_CHANGED_ACTION);
            intentFilter.addAction(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION);
            intentFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
            registerReceiver(receiverWifi, intentFilter);

        super.onResume();
    }
    //--
    class WifiReceiver extends BroadcastReceiver {

        // This method call when number of wifi connections changed
        public void onReceive(Context c, Intent intent) {

            conIntent = intent;
            MyLog.doLog("onReceive");
            //--
            if(intent.getAction().equals(WifiManager.NETWORK_STATE_CHANGED_ACTION)) {
                NetworkInfo networkInfo = intent.getParcelableExtra(WifiManager.EXTRA_NETWORK_INFO);
                MyLog.doLog("#####", networkInfo.toString());
                if (networkInfo.getType() == ConnectivityManager.TYPE_WIFI && networkInfo.isConnected()) {
                    // Wifi is connected
                    MyLog.doLog("connection: ", "Wifi is connected 1: " + String.valueOf(networkInfo));
            ///        Toast.makeText(getApplicationContext(), "Wifi is connected 1 "+networkInfo.getExtraInfo().toString(), Toast.LENGTH_LONG).show();
                } else {
                    MyLog.doLog("connection: ", "Wifi is disconnected 1: " + String.valueOf(networkInfo));
             ///       Toast.makeText(getApplicationContext(), "Wifi is disconnected 1", Toast.LENGTH_LONG).show();
                }

                MyLog.doLog("Network State has changed");
                //Toast.makeText(getApplicationContext(), "Network State has changed", Toast.LENGTH_LONG).show();
                //--
            }
            else if(intent.getAction().equals(ConnectivityManager.CONNECTIVITY_ACTION)){
                NetworkInfo networkInfo = intent.getParcelableExtra(ConnectivityManager.EXTRA_NETWORK_INFO);
                if(networkInfo.getType() == ConnectivityManager.TYPE_WIFI &&
                        ! networkInfo.isConnected()) {
                    // Wifi is disconnected
                    MyLog.doLog("connection", "Wifi is disconnected 2: " + String.valueOf(networkInfo));
                ///    Toast.makeText(getApplicationContext(), "Wifi is disconnected 2", Toast.LENGTH_LONG).show();
                }
            }
            //--
            else if (intent.getAction().equals(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION)){
                MyLog.doLog("Network Scan results available");
        ///        Toast.makeText(getApplicationContext(), "Network Scan results available", Toast.LENGTH_LONG).show();
            }
            connList.clear();
            connStatusList.clear();
            connImgList.clear();

            checkPermissionAndDo(intent);
        }

    }
    //---
    void checkPermissionAndDo(Intent intent){
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED){
            requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
                    PERMISSIONS_REQUEST_CODE_ACCESS_COARSE_LOCATION);
            //After this point you wait for callback in onRequestPermissionsResult(int, String[], int[]) overridden method

        } else if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && checkSelfPermission(Manifest.permission.ACCESS_WIFI_STATE) != PackageManager.PERMISSION_GRANTED){
            requestPermissions(new String[]{Manifest.permission.ACCESS_WIFI_STATE},
                    PERMISSIONS_REQUEST_CODE_ACCESS_WIFI_STATE);
            //After this point you wait for callback in onRequestPermissionsResult(int, String[], int[]) overridden method
        }

        else{
            wifiList.clear();
            wifiList.addAll(mainWifi.getScanResults());
            showConnectionList(intent);
            // permission was previously granted; or legacy device
        }
    }
    //---
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        if(grantResults.length>0){
            if ((requestCode == PERMISSIONS_REQUEST_CODE_ACCESS_COARSE_LOCATION
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) |
                    (requestCode == PERMISSIONS_REQUEST_CODE_ACCESS_WIFI_STATE
                            && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                    )
            {
                wifiList.clear();
                wifiList.addAll(mainWifi.getScanResults());
                showConnectionList(conIntent);
            }
        }

    }
    //--
    void showConnectionList(Intent intent){
        wifiInfo = mainWifi.getConnectionInfo();
        currentSSID = wifiInfo.getSSID();

        // Get current wifi info
        if(null != intent && intent.getAction().equals(WifiManager.NETWORK_STATE_CHANGED_ACTION)){
            NetworkInfo networkInfo = intent.getParcelableExtra(WifiManager.EXTRA_NETWORK_INFO);
//            Toast.makeText(getApplicationContext(), "Wifi SSID "+networkInfo.getExtraInfo().toString(), Toast.LENGTH_LONG).show();
            currentSSID = networkInfo.getExtraInfo().toString();

            if(networkInfo.getType() == ConnectivityManager.TYPE_WIFI && networkInfo.isConnected()){

            } else {
                currentSSID = "";
            }

        }

        mainText.setVisibility(View.GONE);
        mainText.setText("");

        for(int i = 0; i < wifiList.size(); i++){

            connList.add(wifiList.get(i).SSID);
            connStatusList.add(checkConnSecureType(wifiList.get(i), currentSSID));
            connImgList.add(R.drawable.ic_wifi);
        }

        if(null == customAdapter || customAdapter.isEmpty()){
            customAdapter = new CustomAdapter(context, connList, connStatusList, connImgList);
            lv.setAdapter(customAdapter);
            MyLog.doLog("customAdapter Initialization");
        } else{
            MyLog.doLog("customAdapter already Initialized");
        }
        customAdapter.notifyDataSetChanged();
    }

    private String checkConnSecureType(ScanResult con, String currentSSID){
        String connected="";
    ///    Toast.makeText(getApplicationContext(), "con.SSID: "+con.SSID+" ## currentSSID: "+currentSSID, Toast.LENGTH_LONG).show();
        MyLog.doLog("SSID", "current:"+currentSSID+ " con: "+con.SSID);
        if(currentSSID.equals(con.SSID) | currentSSID.equals("\""+con.SSID+"\"")){
            connected = " (Connected)";
        }
        if(currentSSID.toLowerCase().contains("unknown") | currentSSID.toLowerCase().contains("0x") | currentSSID.trim().length()==0){
            connected = "";
        }
    //    Toast.makeText(getApplicationContext(), "Con SSID: "+con.SSID+" # Current SSID "+currentSSID, Toast.LENGTH_LONG).show();

        if(con.capabilities.contains("WPA2")){
            return "Secured with WPA2"+connected;
        }
        else if(con.capabilities.contains("WPA")){
            return "Secured with WPA"+connected;
        }
        else if(con.capabilities.contains("WEP")){
            return "Secured with WEP"+connected;
        }

        return "OPEN"+connected;
    }

    private void goBackHome(){
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(WirelessMain.this, MainActivity.class));
                WirelessMain.this.finish();
                //onBackPressed();
            }
        });
    }

    private void selectNetwork(){

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                TextView name = (TextView) view.findViewById(R.id.textViewConnName);
                TextView status = (TextView) view.findViewById(R.id.textViewConnStatus);

               /* wirelessInfo = new WirelessInfo(wifiList.get(position).SSID,
                        intToIP(mainWifi.getDhcpInfo().ipAddress),
                        intToIP(mainWifi.getDhcpInfo().netmask),
                        intToIP(mainWifi.getDhcpInfo().gateway),
                        intToIP(mainWifi.getDhcpInfo().dns1));*/

                MyLog.doLog("DETAILS ", wifiList.get(position).SSID+"\n"+
                        "SSID: "+wifiList.get(position).SSID+"\n"+
                        "ipaddress: "+intToIP(mainWifi.getDhcpInfo().ipAddress)+"\n"+
                        "gateway: "+intToIP(mainWifi.getDhcpInfo().gateway)+"\n"+
                        "subnet: "+intToIP(mainWifi.getDhcpInfo().netmask)+"\n"+
                        "dns: "+intToIP(mainWifi.getDhcpInfo().dns1)+"\n"+
                        "## ipaddress: "+intToIP(mainWifi.getConnectionInfo().getIpAddress()));
                // --- Selected Network ---
                wifiSetUpDialog(wifiList.get(position));
            }
        });
    }

    String intToIP(int ip){

        String ipStr = String.format("%d.%d.%d.%d",
                (ip & 0xff),
                (ip >> 8 & 0xff),
                (ip >> 16 & 0xff),
                (ip >> 24 & 0xff));

        return ipStr;
    }

    void wifiSetUpDialog(ScanResult selectedNetwork){

        final String SSID, Security;
        String signalStrength;
        int signalLevel;
        SSID = selectedNetwork.SSID;
        signalLevel = mainWifi.calculateSignalLevel(wifiInfo.getRssi(), selectedNetwork.level);
        signalStrength = getSignalStrength(signalLevel);
        Security = checkConnSecureType(selectedNetwork, currentSSID);

        final Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.wireless_setup_dialog);
        //   dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        final TextView connectToVal, signalVal, SecurityVal;
        final EditText passwordVal;
        CheckBox showPassword;
        Button connect, cancel;

        connectToVal = (TextView) dialog.findViewById(R.id.textViewConnectToVal);
        signalVal = (TextView) dialog.findViewById(R.id.textViewSignalVal);
        SecurityVal = (TextView) dialog.findViewById(R.id.textViewSecurityVal);
        passwordVal = (EditText) dialog.findViewById(R.id.editTextPasswordVal);
        showPassword = (CheckBox) dialog.findViewById(R.id.checkBoxShowPassword);
        connect = (Button) dialog.findViewById(R.id.buttonConnect);
        cancel = (Button) dialog.findViewById(R.id.buttonCancel);
        connectToVal.setText(SSID);
        SecurityVal.setText(Security);
        signalVal.setText(""+signalStrength);
        if((SecurityVal.getText().toString().toLowerCase()).contains("connected"))
            connect.setText(getResources().getString(R.string.button_disconnect));

        showPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(((CheckBox)v).isChecked()){
                    passwordVal.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                } else {
                    passwordVal.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                }
            }
        });

        passwordVal.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                //--
                if (keyCode == KeyEvent.KEYCODE_ENTER) {
                    InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                    passwordVal.setFocusable(false);
                    passwordVal.setFocusableInTouchMode(true);
                    return true;
                } else {
                    return false;
                }
                //--
            }
        });

        connect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //-- disconnect --
                if((SecurityVal.getText().toString().toLowerCase()).contains("connected")){
                    disConnectSelectedNetwork(wifiInfo.getSSID());
                    dialog.dismiss();

                } else {
                    //-- connect --
                    String password = passwordVal.getText().toString();
                    boolean connectSuccess = connectToWifiNetwork(SSID, password, Security);
                    validateConState(dialog);
                }
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();

    }
    //--
    private String getSignalStrength(int signalLevel){
        String strength= "";
        if(signalLevel>-50)
            strength= "Excellent";
        else if(-50>signalLevel && signalLevel>-60)
            strength = "Good";
        else if(-60>signalLevel && signalLevel>-70)
            strength = "Fair";
        else if(-70> signalLevel)
            strength = "Weak";
        else strength = "N/A";

        return strength;
    }
    //--
    private boolean connectToWifiNetwork(String SSID, String password, String security){
        String networkSSID = SSID;
        String networkPass = password;

        WifiConfiguration conf = new WifiConfiguration();
        conf.SSID = "\"" + networkSSID + "\"";

        if(security.contains("WEP")){
            MyLog.doLog("security type: ", "WEP");
            conf.wepKeys[0] = "\"" + networkPass + "\"";
            conf.wepTxKeyIndex = 0;
            conf.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.NONE);
            conf.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.WEP40);
        } else if(security.contains("WPA")){
            MyLog.doLog("security type: ", "WPA");
            conf.preSharedKey = "\""+ networkPass +"\"";
            MyLog.doLog("password: ", ""+conf.preSharedKey);

            conf.status = WifiConfiguration.Status.ENABLED;
            conf.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.TKIP);
            conf.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.CCMP);
            conf.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.WPA_PSK);
            conf.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.TKIP);
            conf.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.CCMP);
            conf.allowedProtocols.set(WifiConfiguration.Protocol.RSN);
        } else if(security.contains("OPEN")){
            MyLog.doLog("security type: ", "OPEN");
            conf.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.NONE);
        }

        WifiManager wifiManager = (WifiManager)context.getSystemService(Context.WIFI_SERVICE);
        wifiManager.addNetwork(conf);

        List<WifiConfiguration> list = wifiManager.getConfiguredNetworks();
        for( WifiConfiguration i : list ) {
            if(i.SSID != null && i.SSID.equals("\"" + networkSSID + "\"")) {
                wifiManager.disconnect();
                wifiManager.enableNetwork(i.networkId, true);
                // Permanently save this network
                wifiManager.saveConfiguration();
                boolean connect = wifiManager.reconnect();
                return connect;
            }
        }
        return false;
    }

    void validateConState(final Dialog dialog){
        progressDialog = ProgressDialog.show(WirelessMain.this, "", "Connecting..", true);

        new Thread(){

            public void run(){
                try {
                    String status;
                    boolean success = false;

                    for(int i=0; i<3; i++){
                        sleep(4000);
                        //++
                        status = "";
                        success = false;
                        if(conIntent.getAction().equals(WifiManager.NETWORK_STATE_CHANGED_ACTION)) {
                            NetworkInfo networkInfo = conIntent.getParcelableExtra(WifiManager.EXTRA_NETWORK_INFO);
                            MyLog.doLog("#####", networkInfo.toString());
                            if (networkInfo.getType() == ConnectivityManager.TYPE_WIFI && networkInfo.isConnected()) {
                                // Wifi is connected
                                MyLog.doLog("connection: ", "Wifi is connected 1: " + String.valueOf(networkInfo));
                                status = "Wifi is connected 1 "+networkInfo.getExtraInfo().toString()+" "+i;
                                success = true;
                            } else {
                                MyLog.doLog("connection: ", "Wifi is disconnected 1: " + String.valueOf(networkInfo));
                                status = "Wifi is disconnected 1 "+networkInfo.getExtraInfo().toString()+" "+i;
                            }

                            MyLog.doLog("Network State has changed");
                            //Toast.makeText(getApplicationContext(), "Network State has changed", Toast.LENGTH_LONG).show();
                            //--
                        }
                        else if(conIntent.getAction().equals(ConnectivityManager.CONNECTIVITY_ACTION)){
                            NetworkInfo networkInfo = conIntent.getParcelableExtra(ConnectivityManager.EXTRA_NETWORK_INFO);
                            if(networkInfo.getType() == ConnectivityManager.TYPE_WIFI && networkInfo.isConnected()) {
                                // Wifi is disconnected
                                MyLog.doLog("connection", "Wifi is disconnected 2: " + String.valueOf(networkInfo));
                                status = "Wifi is connected 2 "+networkInfo.getExtraInfo().toString()+" "+i;
                                success = true;
                            }
                            else {
                                MyLog.doLog("connection: ", "Wifi is disconnected: " + String.valueOf(networkInfo));
                                status = "Wifi is disconnected 2 "+networkInfo.getExtraInfo().toString()+" "+i;
                            }
                        }
                        else {
                            status = "other intent";
                        }
                        //++
                        Message msg = new Message();
                        msg.obj = status;
                        handler.sendMessage(msg);

                        if(success){
                            break;
                        }

                    }
                    //--
                    progressDialog.dismiss();
                    if(success){
                        //-- close wireless setup dialog
                        dialog.dismiss();
                        //-- show next Screen
                        Message msg = new Message();
                        msg.obj = "Connected Successfully";
                        handler.sendMessage(msg);

                    } else {
                        //-- Failure
                        Message msg = new Message();
                        msg.obj = "Connection Failure";
                        handler.sendMessage(msg);
                        //-- Failure
                    }

                } catch (Exception e){
                    e.printStackTrace();
                }

            }

        }.start();
    }


    Handler handler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            String message = (String) msg.obj;
            Toast.makeText(getApplicationContext(), ""+message, Toast.LENGTH_SHORT).show();
            if(message.toLowerCase().contains("success")){
                wifiStatusDialog();
            }
        }
    };
    //--
    void wifiStatusDialog(){

        MyLog.doLog("############# wifiStatusDialog #############");
        String ssid , ipAddress, subnetMask, defaultGateway, dns;
        //--
        wirelessInfo = new WirelessInfo(wifiInfo.getSSID(),
                intToIP(mainWifi.getDhcpInfo().ipAddress),
                intToIP(mainWifi.getDhcpInfo().netmask),
                intToIP(mainWifi.getDhcpInfo().gateway),
                intToIP(mainWifi.getDhcpInfo().dns1));

            ssid = wirelessInfo.getSsid();
            ipAddress = wirelessInfo.getIpAddress();
            subnetMask = wirelessInfo.getSubnetMask();
            defaultGateway = wirelessInfo.getDefaultGateway();
            dns = wirelessInfo.getDns();

        //--
        final Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.wireless_status_dialog);

        final TextView ssidVal, ipAddressVal, subnetVal, defaultGatewayVal, dnsVal;
        Button finish, disconnect;

        ssidVal = (TextView) dialog.findViewById(R.id.textViewSSIDVal);
        ipAddressVal = (TextView) dialog.findViewById(R.id.textViewIpAddressVal);
        subnetVal = (TextView) dialog.findViewById(R.id.textViewSubnetVal);
        defaultGatewayVal = (TextView) dialog.findViewById(R.id.textViewDefaultGatewayVal);
        dnsVal = (TextView) dialog.findViewById(R.id.textViewDNSVal);
        finish = (Button) dialog.findViewById(R.id.buttonFinish);
        disconnect = (Button) dialog.findViewById(R.id.buttonDisconnect);

        ssidVal.setText(""+ssid.substring(1, ssid.length()-1));
        ipAddressVal.setText(""+ipAddress);
        subnetVal.setText(""+subnetMask);
        defaultGatewayVal.setText(""+defaultGateway);
        dnsVal.setText(""+dns);

        finish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                startActivity(new Intent(WirelessMain.this, MainActivity.class));
                WirelessMain.this.finish();
            }
        });

        final String ssidToRemove = ssid;
        disconnect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                disConnectSelectedNetwork(ssidToRemove);
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    void disConnectSelectedNetwork(String ssidToRemove){
        WifiManager wifiManager = (WifiManager)context.getSystemService(Context.WIFI_SERVICE);
        wifiManager.disconnect();
        //--
        List<WifiConfiguration> list = wifiManager.getConfiguredNetworks();
        for(WifiConfiguration i: list){
            if(i.SSID.equals(ssidToRemove)){
                wifiManager.removeNetwork(i.networkId);
                wifiManager.saveConfiguration();
                Toast.makeText(getApplicationContext(), "SSID: "+i.SSID+" ## ssidToRemove: "+ssidToRemove, Toast.LENGTH_SHORT).show();
            }
        }
        //--
    }


    // ----------- End of class ---------------------
}
