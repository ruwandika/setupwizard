package au.com.seebo.setupwizard.timezone;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Window;

import com.viewpagerindicator.CirclePageIndicator;
import com.viewpagerindicator.PageIndicator;

import java.util.ArrayList;

import au.com.seebo.setupwizard.R;

public class TimeZoneMain extends FragmentActivity {

    private ViewPager _mViewPager;
    private ViewPagerAdapter _adapter;
    PageIndicator mIndicator;
    CirclePageIndicator circlePageIndicator;
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_timezone_main);

        context = this;
        setUpView();
    }

    private void setUpView()
    {
        _adapter = new ViewPagerAdapter(getApplicationContext(),getSupportFragmentManager());
        _mViewPager = (ViewPager) findViewById(R.id.pager);

        _mViewPager.setAdapter(_adapter);
        circlePageIndicator=(CirclePageIndicator)findViewById(R.id.indicator);
        mIndicator=circlePageIndicator;
        circlePageIndicator.setViewPager(_mViewPager);
        final float density = getResources().getDisplayMetrics().density;
        circlePageIndicator.setBackgroundResource(android.R.color.transparent);
        circlePageIndicator.setRadius(10 * density);
        //circlePageIndicator.setPageColor();
        circlePageIndicator.setFillColor(0xFFFFFFFF);
        //circlePageIndicator.setStrokeColor(0xFF000000);
        //circlePageIndicator.setStrokeWidth(2 * density);
        circlePageIndicator.setSnap(true);
        _mViewPager.setCurrentItem(0);

        setCirclePageIndicator();
    }

    public void setCirclePageIndicator()
    {
        circlePageIndicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                // TODO Auto-generated method stub
                btnAction(position);
            }

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                // TODO Auto-generated method stub
                //btnAction(position);
            }

            @Override
            public void onPageScrollStateChanged(int arg0) {
                // TODO Auto-generated method stub

            }
        });
    }

    private void btnAction(int action){
        switch(action){
            case 0:
                break;
            case 2:
                //startActivity(new Intent(ViewPager_activity.this,HomePage_Activity.class));
                break;
        }
    }

   /* @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        int action = event.getAction();
        int keyCode = event.getKeyCode();
        switch (keyCode){
            case KeyEvent.KEYCODE_DPAD_LEFT:
               sendLeftKeyPressedBroadcast();
               return true;

            case KeyEvent.KEYCODE_DPAD_RIGHT:
                sendRightKeyPressedBroadcast();
                return true;
        }

        return super.dispatchKeyEvent(event);
    }

    private void sendLeftKeyPressedBroadcast(){
        Intent intent = new Intent("left_arrow");
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    private void sendRightKeyPressedBroadcast(){
        Intent intent = new Intent("right_arrow");
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }*/

    //------
}
