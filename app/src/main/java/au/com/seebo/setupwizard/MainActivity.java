package au.com.seebo.setupwizard;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import au.com.seebo.setupwizard.timezone.TimeZoneMain;
import au.com.seebo.setupwizard.wired.WiredMain;
import au.com.seebo.setupwizard.wireless.WirelessMain;

public class MainActivity extends Activity implements View.OnFocusChangeListener, View.OnClickListener{

    ImageButton buttonWireless, buttonWired, buttonTimezone;
    ImageView circleIndicator1, circleIndicator2, circleIndicator3;
    Button close;
    private TextView tv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tv= (TextView)findViewById(R.id.textHeader1);
        buttonWireless = (ImageButton) findViewById(R.id.imageButton);
        buttonWired = (ImageButton) findViewById(R.id.imageButton2);
        buttonTimezone = (ImageButton) findViewById(R.id.imageButton3);
        close = (Button) findViewById(R.id.button_close);
        close.setOnClickListener(this);
        close.setCompoundDrawablesWithIntrinsicBounds(R.drawable.x_mark_32, 0, 0, 0);
        //--
        buttonWireless.setOnFocusChangeListener(this);
        buttonWired.setOnFocusChangeListener(this);
        buttonTimezone.setOnFocusChangeListener(this);
        //--
        buttonWireless.setOnClickListener(this);
        buttonWired.setOnClickListener(this);
        buttonTimezone.setOnClickListener(this);
        //--
        circleIndicator1 = (ImageView) findViewById(R.id.indicator1);
        circleIndicator2 = (ImageView) findViewById(R.id.indicator2);
        circleIndicator3 = (ImageView) findViewById(R.id.indicator3);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {

        if(hasFocus==true & v.getId()==R.id.imageButton){
            circleIndicator1.setBackgroundResource(R.drawable.on_circle);
            circleIndicator2.setBackgroundResource(R.drawable.off_circle);
            circleIndicator3.setBackgroundResource(R.drawable.off_circle);
        } else if(hasFocus==true & v.getId()==R.id.imageButton2){
            circleIndicator1.setBackgroundResource(R.drawable.off_circle);
            circleIndicator2.setBackgroundResource(R.drawable.on_circle);
            circleIndicator3.setBackgroundResource(R.drawable.off_circle);
        } else if(hasFocus==true & v.getId()==R.id.imageButton3){
            circleIndicator1.setBackgroundResource(R.drawable.off_circle);
            circleIndicator2.setBackgroundResource(R.drawable.off_circle);
            circleIndicator3.setBackgroundResource(R.drawable.on_circle);
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.imageButton:
             //   this.finish();
                // Toast.makeText(getApplicationContext(), "WIRELESS", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(MainActivity.this, WirelessMain.class));
                MainActivity.this.finish();
                break;
            case R.id.imageButton2:
                //Toast.makeText(getApplicationContext(), "WIRED", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(MainActivity.this, WiredMain.class));
                MainActivity.this.finish();
                break;
            case R.id.imageButton3:
                //Toast.makeText(getApplicationContext(), "TIMEZONE", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(MainActivity.this, TimeZoneMain.class));
                MainActivity.this.finish();
                break;
            case R.id.button_close:
                onBackPressed();
                break;
        }

    }


    //--
}
