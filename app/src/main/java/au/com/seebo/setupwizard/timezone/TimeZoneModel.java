package au.com.seebo.setupwizard.timezone;

/**
 * Created by Ruwa on 5/16/17.
 */

public class TimeZoneModel {

    private String timeZoneId;
    private String valGMT;
    private boolean isSelected;

    public TimeZoneModel(String timeZoneId, String valGMT) {
        this.timeZoneId = timeZoneId;
        this.valGMT = valGMT;
    }

    public TimeZoneModel(String timeZoneId, String valGMT, boolean isSelected) {
        this.timeZoneId = timeZoneId;
        this.valGMT = valGMT;
        this.isSelected = isSelected;
    }

    public String getTimeZoneId() {
        return timeZoneId;
    }

    public void setTimeZoneId(String timeZoneId) {
        this.timeZoneId = timeZoneId;
    }

    public String getValGMT() {
        return valGMT;
    }

    public void setValGMT(String valGMT) {
        this.valGMT = valGMT;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}
