package au.com.seebo.setupwizard.timezone;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Locale;

import au.com.seebo.setupwizard.MainActivity;
import au.com.seebo.setupwizard.R;

/**
 * Created by Ruwa on 5/16/17.
 */

public class ViewPager_second_fragment extends Fragment {

    private Button finish, back;
    private LanguageAdapter languageAdapter;
    private ListView lv;
    private TextView txtStatus;
    private View root;
    private ArrayList<LanguageModel> languageData;

    public static Fragment newInstance(Context context) {

        ViewPager_second_fragment f = new ViewPager_second_fragment();

        return f;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if(root == null){
            root = inflater.inflate(R.layout.viewpager_second_fragment, container, false);
        } else {
            ViewGroup parent = (ViewGroup) root.getParent();
            parent.removeView(root);
        }
        back = (Button) root.findViewById(R.id.button_back);
        finish = (Button) root.findViewById(R.id.buttonFinish);
        lv = (ListView) root.findViewById(R.id.list);
        txtStatus = (TextView) root.findViewById(R.id.textStatus);
        txtStatus.setText("Loading Locales..");
        doButtonClick();
        selectLocale();

        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        getLocales();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    private void doButtonClick(){
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        finish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), MainActivity.class));
                getActivity().finish();
            }
        });
    }

    private void selectLocale(){
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                for(int i=0; i<languageData.size(); i++){
                    languageData.get(i).setSelected(false);
                }
                languageData.get(position).setSelected(true);
                languageAdapter.notifyDataSetChanged();
                // - Change Locales -
                LanguageModel data = languageData.get(position);
                changeLocale(data.getLanguage().toString(), data.getLocale().toString());

            }
        });
    }

    private void getLocales(){
        new Thread(){
            @Override
            public void run() {
                try {
                    languageData = new ArrayList<>();
                    LanguageModel languageObj;

                    Locale[] locales = Locale.getAvailableLocales();
                    for(Locale l : locales){
                        languageObj = new LanguageModel(l.getDisplayLanguage().toString(),
                                l.getDisplayCountry().toString());

                        languageData.add(languageObj);
                    }
                    handler.sendEmptyMessage(0);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }.start();
    }
    Handler handler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            txtStatus.setText("");
            languageAdapter = new LanguageAdapter(getActivity(), languageData);
            lv.setAdapter(languageAdapter);
            //lv.setSelection(0);
            //languageData.get(0).setSelected(true);
            //languageAdapter.notifyDataSetChanged();
        }
    };

    private void changeLocale(String lang, String country){

        Locale locale = new Locale(lang, country);
        Resources resources = getResources();
        Configuration configuration = resources.getConfiguration();
        DisplayMetrics displayMetrics = resources.getDisplayMetrics();
        configuration.setLocale(locale);
        resources.updateConfiguration(configuration,displayMetrics);

    }



    /*@Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("left_arrow");
        intentFilter.addAction("right_arrow");
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(mMessageReceiver, intentFilter);
    }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            //------ do -------
            if(intent.getAction().equals("left_arrow"))
                back.requestFocus();

            if(intent.getAction().equals("right_arrow"))
                finish.requestFocus();
        }
    };*/

}
